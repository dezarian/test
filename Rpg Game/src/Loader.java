import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;


public class Loader 
{

	static String[] textData;
	
	public static void LoadFile() throws IOException
	{		
		String path = "test.csv";
		
		FileReader fr = new FileReader(path);
		BufferedReader textReader = new BufferedReader(fr);
		
		int numberOfLines = 10;
		textData = new String[numberOfLines];
		
		for(int i = 0; i < numberOfLines; i++)
		{
			textData[i] = textReader.readLine();
						
		}		
	}
	
	public static void PrintFile(int lineNumber)
	{
		System.out.println(textData[lineNumber]);
	}
	
}
