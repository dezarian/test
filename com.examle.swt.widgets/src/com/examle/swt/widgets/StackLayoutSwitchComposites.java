package com.examle.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.graphics.Point;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Text;

public class StackLayoutSwitchComposites {

  static int pageNum = -1;

  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setSize(818, 554);
    shell.setImage(null);
    shell.setMinimumSize(new Point(760, 540));
    shell.setBounds(10, 10, 300, 200);
    // create the composite that the pages will share
    final Composite contentPanel = new Composite(shell, SWT.BORDER);
    contentPanel.setBounds(30, 30, 640, 360);
    final StackLayout layout = new StackLayout();
    contentPanel.setLayout(layout);
    
    
    
    
    
    
        // create the second page's content
        final Composite newGame = new Composite(contentPanel, SWT.NONE);
        newGame.setLayout(new RowLayout());
        
        Label castleImg = new Label(newGame, SWT.NONE);
        castleImg.setImage(SWTResourceManager.getImage("G:\\Eclipse\\com.examle.swt.widgets\\Dynasty Images\\Castle.jpg"));
        
            // create the first page's content
            final Composite options = new Composite(contentPanel, SWT.NONE);
            options.setLayout(new RowLayout());
            Label label = new Label(options, SWT.NONE);
            label.setText("Label on page 1");
            label.pack();

    // create the button that will switch between the pages
    Button pageButton = new Button(shell, SWT.PUSH);
    pageButton.setText("Main Menu");
    pageButton.setBounds(588, 410, 109, 40);
    
    Button btnNewButton = new Button(shell, SWT.NONE);
    btnNewButton.setBounds(309, 409, 123, 43);
    btnNewButton.setText("Load");
    
    Button btnSave = new Button(shell, SWT.NONE);
    btnSave.setText("Options");
    btnSave.setBounds(450, 409, 123, 43);
    
    Button btnExit = new Button(shell, SWT.NONE);
    btnExit.setText("Save");
    btnExit.setBounds(169, 409, 123, 43);
    
    Button btnNewGame = new Button(shell, SWT.NONE);
    btnNewGame.setText("New Game");
    btnNewGame.setBounds(30, 409, 123, 43);
    
    Label background = new Label(shell, SWT.NONE);
    background.setImage(SWTResourceManager.getImage("G:\\Eclipse\\com.examle.swt.widgets\\Dynasty Images\\BackGround.jpg"));
    background.setBounds(0, 0, 744, 501);
    pageButton.addListener(SWT.Selection, new Listener() {
      public void handleEvent(Event event) {
        layout.topControl =  newGame;
        contentPanel.layout();
      }
    });

    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
    display.dispose();
  }
}
